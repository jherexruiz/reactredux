import React, { Component } from 'react';
import PostList from '../containers/post-list'
import PostDetail from '../containers/post-detail'
import PostAdd from '../containers/post-add'
import NotFound from '../containers/NotFound'
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

const App = () => (
  <div className="App">
    <Switch>
        <Route exact path="/" component={PostList} />
        <Route path="/PostDetailPage" component={PostDetail} />
        <Route path="/PostAddPage" component={PostAdd} />
        <Route path="*" component={NotFound} />
    </Switch>
  </div>
);


export default App;
