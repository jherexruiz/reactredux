import React, { Component } from 'react'
import {connect} from 'react-redux'
import { selectPost } from '../actions/index'
import { bindActionCreators } from 'redux'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

class PostList extends Component {
    state = {
        data: []
    }

    componentDidMount() {
        this.loadPost()
    }

    loadPost() {
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(data => {
            this.setState({data: data })
        })
        .catch(err => console.error(this.props.url, err.toString()))
    }

    handleRowSelect = (row, isSelected, e) => {
        const { history, selectPost } = this.props
        selectPost(row)
        history.push("PostDetailPage");
    }

    renderList() {

        const selectRowProp = {
            mode: "radio",
            bgColor: "skyblue",
            hideSelectColumn: true,
            clickToSelect: true,
            onSelect: this.handleRowSelect,
        }

        return (
            <BootstrapTable data={this.state.data} hover striped pagination selectRow={selectRowProp}>
                <TableHeaderColumn dataField="id" isKey={true} dataAlign="center" hidden>ID</TableHeaderColumn>
                <TableHeaderColumn
                    dataField="title"
                    dataSort={true} >Post</TableHeaderColumn>
            </BootstrapTable>
        )
    }
    
    render() {
        return (
            <ul className='list-group col-sm-12'>
                <span align="center"><a href="/PostAddPage" className="active">Add</a></span>
                {this.renderList()}
            </ul>
        )
    }
}

const mapStateToProps = state => state

function mapDispatchToProps(dispatch) {
    return bindActionCreators({selectPost: selectPost}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PostList)