import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from "react-router-dom"
import { ToastContainer, toast } from 'react-toastify'
import { css } from 'glamor'

class PostAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: ''
        };
    
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeDescription = this.handleChangeDescription.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    handleChangeTitle(event) {
        this.setState({title: event.target.value});
    }

    handleChangeDescription(event) {
        this.setState({description: event.target.value});
    }

    handleSubmit(event) {
        const { title, description } = this.state

        if (title === '' || description === '') {
            toast.error("Error, Title or Description cannot be empty !", { autoClose: 2000 });
        } else {
            toast.success("Success, Post !", { autoClose: 2000 });
            fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                body: JSON.stringify({
                  title: title,
                  body: description,
                  userId: 1
                }),
                headers: {
                  "Content-type": "application/json; charset=UTF-8"
                }
              })
              .then(response => response.json())
              .then(json => {
                console.log(json)
                this.props.history.push("");
              })
              
        }
        event.preventDefault();
    }

    render() {
        return (
            <div className="col-12">
                <ToastContainer autoClose={2000}/>
                <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <a href="/" >Back</a>
                    <h3>New Posts</h3><br/>
                    <div className="col-md-1">
                        <label>
                            Title:
                        </label>
                    </div>
                    <div className="col-md-2">
                        <input type="text" name="title" onChange={this.handleChangeTitle} value={this.state.title} />
                    </div>
                </div>
                <div className="row">
                <div className="col-md-1">
                    <label>
                        Description:
                    </label>
                </div>
                <div className="col-md-2">
                    <input type="text" name="description" onChange={this.handleChangeDescription} value={this.state.description}/>
                </div>
                </div>
                <div className="row">
                    <div className="col-md-1">
                        <input type="submit" value="Submit" />
                    </div>
                </div>
                </form>
            </div>
        )
    }
}

// const mapStateToProps = state => state

export default withRouter(PostAdd)