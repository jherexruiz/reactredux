import React, { Component } from 'react'
import { connect } from 'react-redux'

class PostDetail extends Component {
    state = {
        comments: ''
    }

    componentDidMount() {
        this.loadComments()
    }

    loadComments () {
        const { post } = this.props
        fetch(`https://jsonplaceholder.typicode.com/posts/${post.userId}/comments`)
        .then(response => response.json())
        .then(data => {
            this.setState({comments: data })
        })
        .catch(err => console.error(this.props.url, err.toString()))
    }

    render() {
        const { post } = this.props
        const { comments } = this.state

        if(!post || !comments) {
            return <div>Loading...</div>
        }

        return (
            <div className="row">
                <div className="col-md-12" style={{textAlign: 'center'}}>
                    <div><h3 >Post Details Page</h3></div>
                    <br/>
                </div>
                <div className="col-md-12">
                    <a href="/" >Back</a>
                    <h2>{post.title}</h2>
                    <br/> <br/>
                </div>
                <div className="col-md-12">
                    {post.body}
                    <br/> <br/>
                    <br/> <br/>
                </div>
                <div className="col-md-12">
                    <b>Comments:</b>
                    <ul className='list-group col-sm-12'>
                       {comments.map(comment => {
                           return(
                            <div className="col-md-12" style={{border: 'thin solid'}}>
                                <li key={`name${comment.id}`}>
                                    <b>{comment.name}</b>
                                    <div  key={`email${comment.id}`}>{comment.email}</div>
                                    <div key={`body${comment.id}`}>
                                        <br/>
                                        {comment.body}
                                    </div>
                                </li>
                            </div>
                           )
                        })
                       }
                    </ul>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        post: state.activePost
    }
}

export default connect(mapStateToProps)(PostDetail)