export default function(state = null, action) {
	return [
		{title: 'Javascript: The Good Parts', pages: 101},
		{title: 'Harry Potter', pages: 39},
		{title: 'The Dark Tower', pages: 76},
		{title: 'Eloquent Ruby', pages: 1},
	]
}