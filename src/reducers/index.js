import { combineReducers } from 'redux';
import BooksReducer from './reducer_books'
import ActivePost from './reducer_active_post'

const rootReducer = combineReducers({
  books: BooksReducer,
  activePost: ActivePost
});

export default rootReducer;